# Variables en python

## int
num = 5*3
print(type(num)," : ",num)
## float
decimal = 4.5
print(type(decimal))
## str
cadena = "andres"
print(type(cadena))
cadena = 'andres'
print(type(cadena))
cadena = """andres"""
print(type(cadena))
cadena = '''andres'''
print(type(cadena))
print(cadena[:-2])
#print(cadena*100)
## boolean
semaforo = True #False
print(type(semaforo),semaforo)
## date , timestamp

# estructuras de datos
#listas []
listas = [1,cadena]
listas[1]= "felipe"
print(type(listas),listas[1])
listas_dos = [0][0]
# listas_dos.append(0)
# print(type(listas_dos),listas_dos)
#tuplas ()
tupla = ()
tupla=(1,cadena)
print(type(tupla),tupla)

#diccionarios {}

diccionario = {} # llave : valor

diccionario = {
    "andres": 892173873,
    1 : "carlos"
}

print(diccionario,diccionario["andres"],diccionario[1],diccionario.keys())

#operadores aritmeticos
# suma +
a= 5
b=7
print("suma : ",a+b)
# resta -
print("resta : ",a-b)
#multiplacion *
print("multiplacion : ",a*b)
#division /
print("division : ",a/b)
# modulo %
print("modulo : ",a%b)
#exponeciacion **
print("exponenciacion : ",a**3)
# negacion ~
print("negacion : ",~b)

#*********************************************************************************
#*********************************************************************************
#operadores logicos
# igual ==
print("iguales : ", a==b ,a ," == ", b)
#identico
print("identico : ", a is b ,a ," === ", b)
#diferente !=
print("diferente : ", a != b ,a ," != ", b)
#menor <
print("menor : ", a < b ,a ," < ", b)
#mayor >
print("mayor : ", a > b ,a ," > ", b)
#mayor igual >=
print("mayor o igual : ", a >= b ,a ," >= ", b)
#menor igual
print("menor o igual : ", a <= b ,a ," <= ", b)
#pertenece in
lista_num = [1,2,3,4,5,6,7]
print("pertenece : ", a in lista_num ,a ," in ",lista_num)

#conversion de tipos
