
# libreria para manejo de series y Dataframes
import pandas as pd 
import re



class Extraccion:
    def __init__(self):
        self.df = pd.DataFrame()
    
    def extra_xlsx(self,ruta_xlsx=""):
        """
        Extraer información de excel 

        Args:
            ruta_xlsx (str, optional): la ruta del archivo de excel. Defaults to "".

        Returns:
            _type_: DataFrame 
        """
        ruta_xlsx =ruta_xlsx
        df = pd.read_excel(ruta_xlsx,index_col=False)
        if len(df)>0:
            self.df = df
            return df
        return pd.DataFrame()
    
    
    def extra_csv_txt(self,ruta_csv="",delimitador=""):
        """
        Extraer información de excel 

        Args:
            ruta_xlsx (str, optional): la ruta del archivo de excel. Defaults to "".

        Returns:
            _type_: DataFrame 
        """
        ruta_csv =ruta_csv
        delimitador =delimitador
        if len(delimitador)<=1: 
            df = pd.read_csv(ruta_csv,index_col=False,sep=delimitador)
        elif len(delimitador)>1:
            df = pd.read_csv(ruta_csv,index_col=False,sep=r'\s+')
        if len(df)>0:
            self.df = df
            return df
        return pd.DataFrame()
    
    def extra_logs(self,ruta_logs="",regular_espresion=r''):
        log_rege = regular_espresion
        log_filas =[]
        
        with open(ruta_logs,'r') as archivo:
            for linea in archivo:
                cruce = re.match(log_rege,linea)
                if cruce:
                   log_filas.append(cruce.groups()) 
                
        df_logs = pd.DataFrame(log_filas,columns=['fecha','nivel','descripcion'])
        if len(df_logs)>0:
            self.df = df_logs
            return df_logs
        return pd.DataFrame()
    
    def extra_html(self,url="",numero_tabla=0,local=0):
        ruta_out="E:/curso_procesosetl/clase_a_clase/src/procesamiento_etl/static/html/"
        df_html=pd.read_html(url)
        if len(df_html)>0:
            self.df = df_html[numero_tabla]
            if local>0:
                self.df.to_excel("{}pagina.xlsx".format(ruta_out))
            return df_html[numero_tabla]
        return pd.DataFrame()

        


extraer = Extraccion()
# E:/curso_procesosetl/clase_a_clase/src/procesamiento_etl/static/xlsx/bank_reviews3.xlsx
ruta_exel = "E:/curso_procesosetl/clase_a_clase/src/procesamiento_etl/static/xlsx/bank_reviews3.xlsx"
ruta_csv = "E:/curso_procesosetl/clase_a_clase/src/procesamiento_etl/static/csv/bank_reviews3.csv"
caracter=","
extraer = Extraccion()

print("*******************************************************************************")
print("******************************  DAtaFrame Excel           *********************")
print("*******************************************************************************")
print(extraer.extra_xlsx(ruta_xlsx=ruta_exel).head())

print("*******************************************************************************")
print("******************************  DAtaFrame delimitado (csv) ********************")
print("*******************************************************************************")

print(extraer.extra_csv_txt(ruta_csv=ruta_csv,delimitador=caracter).head())

print("*******************************************************************************")
print("******************************  DAtaFrame texto plano (txt) ********************")
print("*******************************************************************************")
# caracter='\s+'
print(extraer.extra_csv_txt(ruta_csv=ruta_csv,delimitador=caracter).head())

print("*******************************************************************************")
print("******************************  DAtaFrame logs app (logs) ********************")
print("*******************************************************************************")
expresion =r'\[(.*?)\] \[(.*?)\] (.*)'
ruta_logs = "E:/curso_procesosetl/clase_a_clase/src/procesamiento_etl/static/logs/Apache_2k.log"

print(extraer.extra_logs(ruta_logs=ruta_logs,regular_espresion=expresion).head())

# df_p = extraer.extra_logs(ruta_logs=ruta_logs,regular_espresion=expresion)
print("*******************************************************************************")
print("******************************  DAtaFrame html (tabla) ********************")
print("*******************************************************************************")
numero_tabla=1
url = "https://en.wikipedia.org/wiki/List_of_countries_by_inflation_rate"


print(extraer.extra_html(url=url,numero_tabla=numero_tabla,local=1).head())